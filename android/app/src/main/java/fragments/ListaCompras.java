package fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.javi.cuernavaca.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.ListAdapter;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import pojos.Producto;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaCompras extends Fragment {


    ViewGroup rootView;
    String ip;
    ListView lista;
    Button limpiar, ruta;

    public ListaCompras() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = (ViewGroup)inflater.inflate(R.layout.fragment_lista_compras, container, false);

        lista = (ListView) rootView.findViewById(R.id.lstCompras);
        limpiar = (Button) rootView.findViewById(R.id.limpiar);
        ruta = (Button) rootView.findViewById(R.id.ruta);

        datos();


        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.deleteAll();
                realm.commitTransaction();
                lista.setAdapter(null);

            }
        });

        ruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), com.example.javi.cuernavaca.Mapa.class);
                startActivity(intent);

            }
        });

        return rootView;
    }


    public void datos(){

        final ArrayList<Producto> listProducto = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Producto> productos = realm.where(Producto.class)
                .findAll();

        for (Producto p: productos){

            listProducto.add(p);
        }



        final ListAdapter adapter = new ListAdapter(lista.getContext(),listProducto);

        lista.setAdapter(adapter);
    }



}
