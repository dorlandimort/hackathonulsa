package fragments;





import android.content.Context;



import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.example.javi.cuernavaca.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.ListAdapter;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import pojos.Producto;


/**
 * A simple {@link Fragment} subclass.
 */
public class Busqueda extends Fragment {


    ViewGroup rootView;
    Button buscar;
    EditText busqueda;
    ListView lista;
    String ip;
    private SwipeRefreshLayout swipeContainer;


    public Busqueda() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup)inflater.inflate(R.layout.fragment_busqueda, container, false);


        RealmConfiguration config = new RealmConfiguration.Builder(this.getContext())
                .name("mydb.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);


        ip = getString(R.string.ip);
        buscar = (Button) rootView.findViewById(R.id.btnBuscar);
        busqueda = (EditText) rootView.findViewById(R.id.buscar);
        lista = (ListView) rootView.findViewById(R.id.lstBusqueda);

        //Al abrir la aplicacion
        inicio();


        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buscar(busqueda.getText().toString());

            }
        });


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               final Producto producto = (Producto) parent.getItemAtPosition(position);

                new LovelyStandardDialog(getContext())
                        .setTopColorRes(R.color.colorPrimaryDark)
                        .setButtonsColorRes(R.color.colorAccent)
                        .setIcon(R.mipmap.ic_action_cart)
                        .setTitle("Confirmación")
                        .setMessage("¿Desea agregar este producto a la canasta?")
                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Realm realm = Realm.getDefaultInstance();
                                realm.beginTransaction();

                                Producto producto1 = realm.copyToRealm(producto);

                                realm.commitTransaction();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();
            }

        });



        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                inicio();
                busqueda.setText("");
                swipeContainer.setRefreshing(false);
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);



        return rootView;
    }




    public void inicio(){


        final ArrayList<Producto> listProducto = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        Context context= getActivity().getApplicationContext();


        client.get(context,ip+"/Hackathon/producto/getProductos", new JsonHttpResponseHandler(){


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONArray jarray = null;
                    jarray = response.getJSONArray("productos");

                    for(int i = 0; i < jarray.length(); ++i) {
                        JSONObject jobj = jarray.getJSONObject(i);
                        Producto producto = new Producto();
                        producto.setTitulo(jobj.getString("nombre"));
                        producto.setDescripcion(jobj.getString("descripcion"));
                        producto.setMercado(jobj.getString("mercado"));
                        producto.setLat(jobj.getDouble("lat"));
                        producto.setLng(jobj.getDouble("lng"));
                        producto.setUrl(jobj.getString("url"));

                        if(jobj.getBoolean("isPromo")){

                            producto.setPrecio("");
                            producto.setCategoria("");
                            producto.setPromo(true);

                        }else{
                            producto.setCategoria(jobj.getString("categoria"));
                            producto.setPrecio("$"+jobj.getString("precio"));
                        }

                        listProducto.add(producto);
                    }

                    final ListAdapter adapter = new ListAdapter(lista.getContext(),listProducto);

                    lista.setAdapter(adapter);



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });



    }

    public void buscar(String buscar){


        final ArrayList<Producto> listProducto = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        Context context= getActivity().getApplicationContext();


        client.get(context,ip+"/Hackathon/producto/find?value="+buscar, new JsonHttpResponseHandler(){


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONArray jarray = null;
                    jarray = response.getJSONArray("productos");

                    for(int i = 0; i < jarray.length(); ++i) {
                        JSONObject jobj = jarray.getJSONObject(i);
                        Producto producto = new Producto();
                        producto.setTitulo(jobj.getString("nombre"));
                        producto.setDescripcion(jobj.getString("descripcion"));
                        producto.setUrl(jobj.getString("url"));
                        if(jobj.getBoolean("isPromo")){

                            producto.setPrecio("");
                            producto.setCategoria("");
                            producto.setPromo(true);

                        }else{
                            producto.setCategoria(jobj.getString("categoria"));
                            producto.setPrecio("$"+jobj.getString("precio"));
                        }

                        listProducto.add(producto);
                    }

                    final ListAdapter adapter = new ListAdapter(lista.getContext(),listProducto);

                    lista.setAdapter(adapter);



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });



    }





}

