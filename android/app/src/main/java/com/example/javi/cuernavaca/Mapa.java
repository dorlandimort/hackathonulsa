package com.example.javi.cuernavaca;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import adapters.ListAdapter;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import pojos.Producto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Mapa extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    GoogleMap googleMap;
    MapView mMapView;
    Button terminar;
    ListView lista;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        mMapView = (MapView) findViewById(R.id.MapView);

        if(mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);

        }



        terminar = (Button) findViewById(R.id.terminar);

        terminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.deleteAll();
                realm.commitTransaction();

                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);

            }
        });

    }


    @Override
    public void onMapReady(GoogleMap mMap) {




        MapsInitializer.initialize(getApplicationContext());
        googleMap = mMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }


        getMyLocation();


    }



    @Override
    public void onLocationChanged(Location location) {



    }

    @Override
    public void onProviderDisabled(String provider) {


    }

    @Override
    public void onProviderEnabled(String provider) {


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    public void getMyLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            locationManager = (LocationManager) this.getSystemService(getApplicationContext().LOCATION_SERVICE);

            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            final String provider = locationManager.getBestProvider(criteria, true);


            locationManager.requestLocationUpdates(provider, 60000, 300, this);

            Location lc = locationManager.getLastKnownLocation(provider);

            if (lc != null && googleMap != null) {

                onLocationChanged(lc);

                leerDatos(lc);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lc.getLatitude(), lc.getLongitude()), 15));

            }else if(lc != null){
                onLocationChanged(lc);
            }

        }


    }


    public void leerDatos(Location lc){



        Realm realm = Realm.getDefaultInstance();
        RealmResults<Producto> productos = realm.where(Producto.class)
                .findAll();


        for (int i=0; i< productos.size(); i++ ){

            if(i == 0){
                trazarRuta(lc.getLatitude(),lc.getLongitude(),productos.get(i).getLat(),productos.get(i).getLng());
                agregarMarca(productos.get(i));
            }else{

                trazarRuta(productos.get(i-1).getLat(),productos.get(i-1).getLng(),productos.get(i).getLat(),productos.get(i).getLng());
                System.out.println(productos.get(i).getLat()+"aaaaaaaaaaaaaaaaaa");
                agregarMarca(productos.get(i));
            }


        }



    }

    public void trazarRuta(double latO, double lngO, double latD, double lngD){


            String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latO + "," + lngO + "&destination=" + latD + "," + lngD;


            Context context = this.getApplicationContext();

            AsyncHttpClient client = new AsyncHttpClient();
            client.get(context, url, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    // If the response is JSONObject instead of expected JSONArray
                    JSONArray route = null;
                    try {
                        route = response.getJSONArray("routes");
                        JSONObject objRoute = route.getJSONObject(0);
                        JSONObject overview = objRoute.getJSONObject("overview_polyline");
                        String polylineString = overview.getString("points");
                        List<LatLng> decodedPoints = PolyUtil.decode(polylineString);
                        PolylineOptions options = new PolylineOptions();
                        options.width(6);
                        options.color(R.color.colorAccent);
                        options.addAll(decodedPoints);

                        googleMap.addPolyline(options);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


            });

    }

    public void agregarMarca(Producto producto){

        LatLng marca = new LatLng(producto.getLat(),producto.getLng());
        googleMap.addMarker(new MarkerOptions().position(marca).title(producto.getMercado()));

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(marca).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


}
