package com.example.javi.cuernavaca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void loguear(View view){
        Intent i = new Intent(this, MainActivity.class);
        finish();
        startActivity(i);
    }

}
