package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.javi.cuernavaca.R;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import pojos.Producto;

/**
 * Created by javier on 3/05/17.
 */

public class ListAdapter extends ArrayAdapter<Producto> {

    public ListAdapter(Context context, ArrayList<Producto> productos) {
        super(context,0,productos);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        Producto producto = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listado, parent, false);
        }
        TextView titulo = (TextView) convertView.findViewById(R.id.titulo);
        titulo.setText(producto.getTitulo());

        TextView descripcion = (TextView) convertView.findViewById(R.id.descripcion);
        descripcion.setText(producto.getDescripcion());

        TextView precio = (TextView) convertView.findViewById(R.id.precio);
        precio.setText(producto.getPrecio());

        TextView mercado = (TextView) convertView.findViewById(R.id.mercado);
        mercado.setText(producto.getMercado());

        TextView promo = (TextView) convertView.findViewById(R.id.promo);
        if(producto.isPromo()){

            promo.setText("PROMO");
        }else{

            promo.setText("");
        }

        ImageView imagen = (ImageView)convertView.findViewById(R.id.imagen);
        Glide.with(getContext())
                .load(producto.getUrl())
                .into(imagen);


        return convertView;
    }
}
