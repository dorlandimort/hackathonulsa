import mx.edu.ulsaoaxaca.hackathon.*
class BootStrap {

    def init = { servletContext ->
        Usuario admin = new Usuario(username: 'admin', password: 'admin', enabled: true).save()
        Usuario mercadoUsuario = new Usuario(username: 'mercado', password: 'mercado', enabled: true).save()
        Rol rol = new Rol(authority: 'ROLE_ADMIN').save()
        Rol rolMercado = new Rol(authority: 'ROLE_MERCADO').save()

        UsuarioRol.create admin, rol, true
        UsuarioRol.create mercadoUsuario, rolMercado, true

        Institucion institucion = new Institucion(nombre: 'Instituto de comercio de Oaxaca',
        descripcion: 'Institucion de prueba').save()

        Zona oaxacaCentro = new Zona(nombre: 'Zona centro')

        institucion.addToZonas(oaxacaCentro)
        institucion.save()

        Mercado mercado = new Mercado(nombre: 'Mercado 20 de Noviembre',
                descripcion: 'Mercado céntrico de la cuidad de Oaxaca', lat: 17.057798, lng: -96.727313,

            direccion: '20 de Noviembre', usuario: mercadoUsuario)


        oaxacaCentro.addToMercados(mercado)
        oaxacaCentro.save()

        Categoria ropaTipica = new Categoria(nombre: 'Ropa típica')
        oaxacaCentro.addToCategorias(ropaTipica)
        oaxacaCentro.save()

        Producto guayabera = new Producto(nombre: 'Guayabera',
                descripcion: 'Color azul', categoria: ropaTipica, precio: 100.0, mercado: mercado, autorizado: false)
        mercado.addToProductos(guayabera)
        mercado.save()


    }
    def destroy = {
    }
}
