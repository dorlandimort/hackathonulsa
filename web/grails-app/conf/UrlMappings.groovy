class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller: 'zona', action: 'mercados')
		"500"(view:'/error')
	}
}
