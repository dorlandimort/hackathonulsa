<!-- Modal -->
<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Editar promocion</h4>
            </div>
            <form action="${createLink(controller: 'promocion', action: 'editar', id: "${promocion.id}")}" method="post" class="form-horizontal ">

                <div class="modal-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-user"></i> Datos de la promocion</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 col-sm-3">Nombre de la promocion:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" maxlength="20" value="${promocion.nombre}" class="form-control" id="nombre" name="nombre" placeholder="Nombre del Producto" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="col-md-3 col-sm-3">Descripcion:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" maxlength="70" value="${promocion.descripcion}" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>