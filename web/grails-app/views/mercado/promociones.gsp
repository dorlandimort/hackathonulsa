<%--
  Created by IntelliJ IDEA.
  User: alejandro
  Date: 3/05/17
  Time: 01:10 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="dashboard">
    <title></title>
</head>

<body>
<div class="col-md-2 col-sm-8 col-xs-8">
    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#crear">
        <i class="fa fa-file"></i> Nueva Promocion
    </button>
</div>
<div class="x_content col-md-8 col-sm-8">
    <table id="example" class="table table-striped table-hover table-condensed responsive-utilities jambo_table">
        <thead>
        <tr class="headings">
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Autorizado</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${promociones}" var="p">
            <tr>
                <td>${p.nombre}</td>
                <td>${p.descripcion}</td>
                <g:if test="${p.autorizado}">
                    <td>Aprobado</td>
                </g:if>
                <g:else>
                    <td>No Aprobado</td>
                </g:else>
                <td>
                    <button onclick="Promocion.editar(${p.id})" class=" btn btn-sm btn-primary"> Editar</button>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Crear nueva promoción</h4>
            </div>
            <form action="${createLink(controller: 'promocion', action: 'guardar')}" method="post" class="form-horizontal ">

                <div class="modal-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-user"></i> Datos de la promoción</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 col-sm-3">Nombre de la promoción:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" maxlength="20" class="form-control" id="nombre" name="nombre" placeholder="Nombre del Producto" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="col-md-3 col-sm-3">Descripción:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" maxlength="70"  class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="modal"></div>
<g:javascript>

        $(document).ready(function() {
            var oTable = $('#example').dataTable({
                "pageLength": 10,
                "language": {
                    "decimal":        "",
                    "emptyTable":     "No hay información disponible",
                    "info":           "Mostrando del _START_ al _END_ de _TOTAL_ entradas",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 entradas",
                    "infoFiltered":   "(filtrado de _MAX_ total entradas)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se han encontrado resultados",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": Activar ordenación ascendente",
                        "sortDescending": ": Activar ordenacion descendente"
                    }
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers"
            });
        });


        $("#mensaje").delay(5000).fadeOut(400);
        $("#warning").delay(7000).fadeOut(400);
        $("#error").delay(10000).fadeOut(400);

        Promocion = {
            editar: function(promocion) {
                $.ajax({
                    type: 'post',
                    url: "${createLink(controller: 'promocion', action: '_modalEditar')}",
                    data: {
                        id: promocion
                    },
                    success: function(result) {
                        $("#modal").html(result);
                        $("#modalEditar").modal('toggle');
                    },
                    error: function(error) {
                        bootbox.alert(error);
                    }
                });
            }
        }

</g:javascript>
</body>
</html>
