<!-- Modal -->
<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Editar usuario</h4>
            </div>
            <form action="${createLink(controller: 'producto', action: 'editar', id: "${producto.id}")}" method="post" class="form-horizontal ">

                <div class="modal-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-user"></i> Datos del Producto</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 col-sm-3">Nombre del producto:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" maxlength="20" value="${producto.nombre}" class="form-control" id="nombre" name="nombre" placeholder="Nombre del Producto" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="col-md-3 col-sm-3">Descripción:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" maxlength="70" value="${producto.descripcion}"  class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="precio" class="col-md-3 col-sm-3">Precio:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="number" step="any" value="${producto.precio}" class="form-control" id="precio" name="precio" placeholder="Precio" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3">Categoría:</label>
                                <div class="col-md-8 col-sm-5">
                                    <g:select class="form-control" id="type" name='categoria'
                                              value="${producto.categoria.id}"
                                              noSelection="${['':'Seleccionar categoria']}"
                                              from='${categorias}'
                                              optionKey="id" optionValue="nombre"></g:select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
