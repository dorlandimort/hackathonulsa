<%--
  Created by IntelliJ IDEA.
  User: giovanni
  Date: 3/05/17
  Time: 09:39 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='dashboard'/>
    <title></title>

</head>

<body>
<div class="input-group top_search col-md-6">
    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar...">

</div>

<div class="x_content">
    <div class="ln_solid"></div>
    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#crear">
        <i class="fa fa-file"></i> Nueva categoría
    </button>
    <br>
    <table id="categorias" class="table table-striped responsive-utilities jambo_table">
        <thead>
        <tr class="headings">
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${zona?.categorias}" var="categoria">
            <tr>
                <td>${categoria?.id}</td>
                <td>${categoria?.nombre}</td>
                <td>${categoria?.descripcion}</td>
                <td><button type="button" class="btn btn-default btn-xs " onclick="Categoria.editar(${categoria?.id})"><i class="fa fa-edit"></i></button></td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Crear nueva categoría</h4>
            </div>
            <form action="${createLink(controller: 'zona', action: 'guardarCategoria')}" method="post" class="form-horizontal ">

                <div class="modal-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-user"></i> Datos Generales</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 col-sm-3">Nombre:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="col-md-3 col-sm-3">Descripción:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="descripcion" name="descripcion" placeholder="Descripción del mercado (opcional)">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="modal"></div>
<g:javascript>

$(document).ready(function() {


        function filterGlobal () {
        $('#categorias').DataTable().search(
            $('#buscar').val()
        ).draw();
}

        $('#buscar').on( 'keyup click', function () {
            filterGlobal();
        } );



        var table = $('#categorias').DataTable( {

            "autoWidth": true,
            "processing": true,
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },


            ],

            "pageLength": 10,
            "language": {
                "decimal":        "",
                "emptyTable":     "No hay información disponible",
                "info":           "Mostrando del _START_ al _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 de 0 de 0 entradas",
                "infoFiltered":   "(filtrado de un total de _MAX_ entradas)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No se han encontrado resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar ordenación ascendente",
                    "sortDescending": ": Activar ordenación descendente"
                }
            },
            "sPaginationType": "full_numbers"


        } );


        Categoria = {
            editar: function(categoria) {
                $.ajax({
                    type: 'post',
                    url: "${createLink(controller: 'zona', action: '_modalEditarCategoria')}",
                    data: {
                        id: categoria
                    },
                    success: function(result) {
                        $("#modal").html(result);
                        $("#editar").modal('toggle');
                    },
                    error: function(error) {
                        bootbox.alert("Error de conexion al servidor");
                    }
                });
            }
        };

    } );




</g:javascript>
</body>
</html>
