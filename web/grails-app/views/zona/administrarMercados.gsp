<%--
  Created by IntelliJ IDEA.
  User: giovanni
  Date: 3/05/17
  Time: 07:49 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name='layout' content='dashboard'/>
    <title></title>

</head>

<body>
<div class="input-group top_search col-md-6">
    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar...">

</div>

<div class="x_content">
    <div class="ln_solid"></div>
    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#crear">
        <i class="fa fa-file"></i> Nuevo mercado
    </button>
    <br>
    <table id="mercados" class="table table-striped responsive-utilities jambo_table">
        <thead>
        <tr class="headings">
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Dirección</th>
            <th>Opciones</th>

        </tr>
        </thead>
        <tbody>
            <g:each in="${zona?.mercados}" var="mercado">
                <tr>
                    <td>${mercado?.id}</td>
                    <td>${mercado?.nombre}</td>
                    <td>${mercado?.descripcion}</td>
                    <td>${mercado?.direccion}</td>
                    <td><button type="button" class="btn btn-default btn-xs " onclick="Mercado.editar(${mercado?.id})"><i class="fa fa-edit"></i></button></td>
                </tr>
            </g:each>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Crear nuevo mercado</h4>
            </div>
            <form action="${createLink(controller: 'zona', action: 'guadarMercado')}" method="post" class="form-horizontal ">

                <div class="modal-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-user"></i> Datos Generales</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 col-sm-3">Nombre:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="col-md-3 col-sm-3">Descripción:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="descripcion" name="descripcion" placeholder="Descripción del mercado (opcional)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="direccion" class="col-md-3 col-sm-3">Dirección:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="direccion" name="direccion" placeholder="Dirección del mercado" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Datos escolares-->
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-sign-in"></i> Localización</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Selecciona la ubicación</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="map" style="height:350px;"></div>
                                    <input type="hidden" id="latitud" name="lat">
                                    <input type="hidden" id="longitud" name="lng">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Datos de acceso-->
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-sign-in"></i> Datos de administrador</h2>

                            <div class="clearfix"></div>
                        </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label for="username" class="col-md-3 col-sm-3">Nombre de usuario:</label>
                            <div class="col-md-8 col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="username" name="username" required placeholder="Nombre de usuario (requerido)">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-danger" id="btnCheck" disabled
                                                data-toggle="tooltip" data-placement="top">
                                            <i id="check" class="fa fa-remove"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3">Contraseña:</label>
                            <div class="col-md-8 col-sm-5">
                                <div class="input-group">
                                    <input type="password" id="password" class="form-control" name="password" required
                                           placeholder="Contraseña del usuario (requerido)">
                                    <span class="input-group-btn">
                                        <button id="showPass" type="button" class="btn btn-default"
                                                data-toggle="tooltip" data-placement="right" title="Ver contraseña">
                                            <i id="show" class="fa fa-eye"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>

            </form>
        </div>
    </div>
</div>
<div id="modal"></div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCv3YXTkGEMvRlvGgQS2ZXWWczCXHsT9vc&callback=initMap"
        async defer></script>
<g:javascript>

$(document).ready(function() {
    var hidden = true;

        var emailValido = true;
        var usuarioValido = false;
        var emailValidoEdit = true;
        var usuarioValidoEdit = false;


        function filterGlobal () {
        $('#mercados').DataTable().search(
            $('#buscar').val()
        ).draw();
}

        $('#buscar').on( 'keyup click', function () {
            filterGlobal();
        } );



        var table = $('#mercados').DataTable( {

            "autoWidth": true,
            "processing": true,
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },


            ],

            "pageLength": 10,
            "language": {
                "decimal":        "",
                "emptyTable":     "No hay información disponible",
                "info":           "Mostrando del _START_ al _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 de 0 de 0 entradas",
                "infoFiltered":   "(filtrado de un total de _MAX_ entradas)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No se han encontrado resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar ordenación ascendente",
                    "sortDescending": ": Activar ordenación descendente"
                }
            },
            "sPaginationType": "full_numbers"


        } );


        Mercado = {
            editar: function(mercado) {
                $.ajax({
                    type: 'post',
                    url: "${createLink(controller: 'zona', action: '_modalEditarMercado')}",
                    data: {
                        id: mercado
                    },
                    success: function(result) {
                        $("#modal").html(result);
                        validarModal();
                        $("#editar").modal('toggle');
                        // Google maps
                        initMapEdit();

                        function initMapEdit() {
                            console.log("edit");
                            var mapDiv = document.getElementById('mapEdit');
                            var location;
                            // Oaxaca
                            // lat 17.062036080716222
                            // lng -96.7294692993164
                            try {
                                var latitude = parseFloat($("#latitudEdit").val());
                                var longitude = parseFloat($("#longitudEdit").val());
                                var name = $("#nombre").val();
                                var marker;

                                if (latitude && longitude)
                                    location = {lat: latitude, lng: longitude};
                                else
                                    location = {lat : 17.062036080716222, lng: -96.7294692993164};

                                var map = new google.maps.Map(mapDiv, {
                                    center : location,
                                    zoom : 10
                                });
                                if (latitude && longitude) {
                                    marker = new google.maps.Marker({
                                        position : location,
                                        map : map,
                                        title : name
                                    });
                                }
                                map.addListener("click", function (event) {
                                    createMarker(event.latLng);
                                });

                                function createMarker(location) {
                                    var title = $("#nombre").val();
                                    if (marker)
                                        marker.setPosition(location);
                                    else {
                                        marker = new google.maps.Marker({
                                            position : location,
                                            map : map,
                                            title: title
                                        });
                                    }
                                    $("#latitudEdit").val(location.lat());
                                    $("#longitudEdit").val(location.lng());
                                };





                            } catch(error) {
                                $(mapDiv).append("<h4>Sin información disponible, verifique su conexión.</h4>");
                            }
                        }
                        // Termina Google Maps

                    },
                    error: function(error) {
                        bootbox.alert("Error de conexion al servidor");
                    }
                });
            }
        };

        $("#showPass").click(function() {
            if (hidden) {
                $("#show").removeClass('fa-eye');
                $("#show").addClass('fa-eye-slash');
                $("#password").attr('type', 'text');
                $("#showPass").attr('title', 'Ocultar contraseña');
                hidden = false;
            } else {
                $("#show").removeClass('fa-eye-slash');
                $("#show").addClass('fa-eye');
                $("#password").attr('type', 'password');
                $("#showPass").attr('title', 'Ver contraseña');
                hidden = true;
            }
        });

        $('#username').on( 'keyup click', function () {
            validarUsuario();
        } );

        function validarUsuario() {
            var usuario = $("#username").val();
            if (usuario != '') {
                $.ajax({
                    type: 'post',
                    url: "${createLink(controller: 'usuario', action: 'validarUsuario')}",
                    data: {
                        username: usuario
                    },
                    success: function (result) {
                        if (result == 'true') {
                            usuarioValido = true;
                            $("#btnCheck").removeClass('btn-danger');
                            $("#btnCheck").addClass('btn-success');
                            $("#check").removeClass('fa-remove');
                            $("#check").addClass('fa-check');
                            $("#btnCheck").attr('title', 'El usuario está disponible')
                        } else {
                            usuarioValido = false;
                            $("#btnCheck").removeClass('btn-success');
                            $("#btnCheck").addClass('btn-danger');
                            $("#check").removeClass('fa-check');
                            $("#check").addClass('fa-remove');
                            $("#btnCheck").attr('title', 'Usuario no válido')
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                usuarioValido = false;
                $("#btnCheck").removeClass('btn-success');
                $("#btnCheck").addClass('btn-danger');
                $("#check").removeClass('fa-check');
                $("#check").addClass('fa-remove');
                $("#btnCheck").attr('title', 'Usuario no válido')
            }

        }

        $('form').on('submit', function(e) {
            if (! usuarioValido) {
                e.preventDefault();
                showError("Error, verifique que el usuario  sea válido antes de continuar")
                return false;
            }
        })

        function showError(error) {

            var notice = new PNotify({
                title : 'Error',
                text : error + '',
                animate : {
                    animate : true,
                    in_class : 'slideInDown',
                    out_class : 'slideOutUp'
                },
                buttons : {
                    closer : false,
                    sticker : false
                },
                type : 'error',
                icon : 'icon-warning-sign',
                delay : 5000
            })

            notice.get().click(function() {
                notice.remove();
            });
        };

        function validarModal() {
            validarUsuarioEdit();
            $("#showPassEdit").click(function() {
                if (hidden) {
                    $("#showEdit").removeClass('fa-eye');
                    $("#showEdit").addClass('fa-eye-slash');
                    $("#passwordEdit").attr('type', 'text');
                    $("#showPassEdit").attr('title', 'Ocultar contraseña');
                    hidden = false;
                } else {
                    $("#showEdit").removeClass('fa-eye-slash');
                    $("#showEdit").addClass('fa-eye');
                    $("#passwordEdit").attr('type', 'password');
                    $("#showPassEdit").attr('title', 'Ver contraseña');
                    hidden = true;
                }
            });

            $('#usernameEdit').on( 'keyup click', function () {
                validarUsuarioEdit();
            } );

            function validarUsuarioEdit() {
                var usuario = $("#usernameEdit").val();
                var id = $("#usid").val();
                if (usuario != '') {
                    $.ajax({
                        type: 'post',
                        url: "${createLink(controller: 'usuario', action: 'validarUsuario')}",
                        data: {
                            username: usuario,
                            id: id
                        },
                        success: function (result) {
                            if (result == 'true') {
                                usuarioValidoEdit = true;
                                $("#btnCheckEdit").removeClass('btn-danger');
                                $("#btnCheckEdit").addClass('btn-success');
                                $("#checkEdit").removeClass('fa-remove');
                                $("#checkEdit").addClass('fa-check');
                                $("#btnCheckEdit").attr('title', 'El usuario está disponible')
                            } else {
                                usuarioValidoEdit = false;
                                $("#btnCheckEdit").removeClass('btn-success');
                                $("#btnCheckEdit").addClass('btn-danger');
                                $("#checkEdit").removeClass('fa-check');
                                $("#checkEdit").addClass('fa-remove');
                                $("#btnCheckEdit").attr('title', 'Usuario no válido')
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                } else {
                    usuarioValidoEdit = false;
                    $("#btnCheckEdit").removeClass('btn-success');
                    $("#btnCheckEdit").addClass('btn-danger');
                    $("#checkEdit").removeClass('fa-check');
                    $("#checkEdit").addClass('fa-remove');
                    $("#btnCheckEdit").attr('title', 'Usuario no válido')
                }

            }
            $('edit').on('submit', function(e) {
                if (! usuarioValidoEdit) {
                    e.preventDefault();
                    showError("Error, verifique que el usuario sea válido antes de continuar");
                    return false;
                }
            })
        }

    } );

// Google Maps

            function initMap() {
                var mapDiv = document.getElementById('map');
                var marker;

                try {
                    var map = new google.maps.Map(mapDiv, {
                        center : {lat: 17.060066796485128, lng: -96.72431945800781},
                        zoom : 8
                    });

                    map.addListener("click", function (event) {
                        createMarker(event.latLng);
                    });

                    function createMarker(location) {
                        if (marker)
                            marker.setPosition(location);
                        else {
                            marker = new google.maps.Marker({
                                position : location,
                                map : map
                            });
                        }
                        $("#latitud").val(location.lat());
                        $("#longitud").val(location.lng());
                    };
                } catch(error) {
                    $(mapDiv).append("<h4>Sin información disponible, verifique su conexión.</h4>");
                }

            }
        // Termina Google Maps


</g:javascript>
</body>
</html>
