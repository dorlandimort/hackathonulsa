<!-- Modal -->
<div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar mercado</h4>
            </div>
            <form action="${createLink(controller: 'zona', action: 'actualizarMercado')}" method="post" class="form-horizontal " id="edit">
                <input type="hidden" name="id" value="${mercado?.id}">
                <input type="hidden" id="usid" value="${mercado?.usuario?.id}">
                <div class="modal-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-user"></i> Datos Generales</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 col-sm-3">Nombre:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" value="${mercado?.nombre}"  class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="col-md-3 col-sm-3">Descripción:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" value="${mercado?.descripcion}"  class="form-control" id="descripcion" name="descripcion" placeholder="Descripción del mercado (opcional)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="direccion" class="col-md-3 col-sm-3">Dirección:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text" value="${mercado?.direccion}" class="form-control" id="direccion" name="direccion" placeholder="Dirección del mercado" required>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- Datos escolares-->
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-sign-in"></i> Localización</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ubicación</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="mapEdit" style="height:350px;"></div>
                                    <input type="hidden" id="latitudEdit" name="lat" value="${mercado?.lat}">
                                    <input type="hidden" id="longitudEdit" name="lng" value="${mercado?.lng}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Datos de acceso-->
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-sign-in"></i> Datos de administrador</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="usernameEdit" class="col-md-3 col-sm-3">Nombre de usuario:</label>
                                <div class="col-md-8 col-sm-5">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="usernameEdit" name="username" required
                                               placeholder="Nombre de usuario (requerido)" value="${mercado?.usuario?.username}">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger" id="btnCheckEdit" disabled
                                                    data-toggle="tooltip" data-placement="top">
                                                <i id="checkEdit" class="fa fa-remove"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3">Contraseña:</label>
                                <div class="col-md-8 col-sm-5">
                                    <div class="input-group">
                                        <input type="password" id="passwordEdit" class="form-control" name="password"
                                               placeholder="Dejar en blanco para no cambiarla">
                                        <span class="input-group-btn">
                                            <button id="showPassEdit" type="button" class="btn btn-default"
                                                    data-toggle="tooltip" data-placement="right" title="Ver contraseña">
                                                <i id="showEdit" class="fa fa-eye"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
