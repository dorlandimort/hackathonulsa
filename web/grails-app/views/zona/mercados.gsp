<%--
  Created by IntelliJ IDEA.
  User: giovanni
  Date: 3/05/17
  Time: 06:55 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='dashboard'/>
    <title></title>

</head>

<body>
<div class="x_content">
    <div class="ln_solid"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="x_content">
                    <div class="row">
                        <g:each in="${zona?.mercados}" var="mercado">
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-shopping-cart"></i>
                                    </div>
                                        <div class="count">${mercado?.id}</div>
                                    <h5>${mercado?.nombre}</h5>
                                    <g:if test="${mercado?.pendientes() > 0}">
                                        <g:if test="${mercado?.pendientes() > 1}">
                                            <a href="${createLink(controller: 'zona', action: 'validacionProductos')}"><p><i class="red">${mercado?.pendientes()} productos pendientes de autorizar</i></p></a>
                                        </g:if>
                                        <g:else>
                                            <a href="${createLink(controller: 'zona', action: 'validacionProductos')}"><p><i class="red">${mercado?.pendientes()} producto pendiente de autorizar</i></p></a>
                                        </g:else>
                                    </g:if>

                                </div>
                            </div>
                        </g:each>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Crear nuevo Aspirante en el periodo actual</h4>
            </div>
            <form action="${createLink(controller: 'aspirante', action: 'guardar')}" method="post" class="form-horizontal ">

                <div class="modal-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-user"></i> Datos Personales</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 col-sm-3">Nombre:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ficha" class="col-md-3 col-sm-3">Ficha:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="ficha" name="ficha" placeholder="Ficha" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="matricula" class="col-md-3 col-sm-3">Matrícula:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="matricula" name="matricula" placeholder="Matrícula" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edad" class="col-md-3 col-sm-3">Edad:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="number" min="10" max="99" class="form-control" id="edad" name="edad" placeholder="Edad" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3">Sexo:</label>
                                <div class="col-md-8 col-sm-5">
                                    <div class="radio">
                                        <label class="radio-inline">
                                            <input type="radio" name="sexo" id="masculino" value="true" checked>
                                            Masculino
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sexo" id="femenino" value="false">
                                            Femenino
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Datos escolares-->
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-sign-in"></i> Datos de Adimisión</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label for="programa" class="col-md-3 col-sm-3">Programa:</label>
                                <div class="col-md-8 col-sm-5">
                                    <g:select  class="form-control" from="${programas}" id="programa" name="programaId"
                                               optionValue="nombre"
                                               optionKey="id">

                                    </g:select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="aulaExamen" class="col-md-3 col-sm-3">Aula de examen:</label>
                                <div class="col-md-8 col-sm-5">
                                    <g:select  class="form-control" from="${aulas}" id="aulaExamen" name="aulaExamen"
                                               optionValue="nombre"
                                               optionKey="id">

                                    </g:select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="escuelaProcedencia" class="col-md-3 col-sm-3">Escuela de procedencia:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="text"  class="form-control" id="escuelaProcedencia" name="escuelaProcedencia" placeholder="Escuela de Procedencia" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="anioDeInteres" class="col-md-3 col-sm-3">Año de Interés:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="number" min="1" max="6" class="form-control" id="anioDeInteres" name="anioDeInteres" placeholder="Año de interes" value="1" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="semestre" class="col-md-3 col-sm-3">Semestre:</label>
                                <div class="col-md-8 col-sm-5">
                                    <input type="number" min="1" max="12" class="form-control" id="semestre" name="semestre" placeholder="Semestre" value="1" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="foraneo" class="col-md-3 col-sm-3"></label>
                                <div class="col-md-8 col-sm-5">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="foraneo" name="foraneo"> Foráneo
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="egresadoUlsa" class="col-md-3 col-sm-3"></label>
                                <div class="col-md-8 col-sm-5">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="egresadoUlsa" name="egresadoUlsa"> Egresado ULSA
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="paseDirecto" class="col-md-3 col-sm-3"></label>
                                <div class="col-md-8 col-sm-5">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="paseDirecto" name="paseDirecto"> Pase Directo
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="necesitaAtencion" class="col-md-3 col-sm-3"></label>
                                <div class="col-md-8 col-sm-5">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="necesitaAtencion" name="necesitaAtencion"> Necesita Atención
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="observaciones" class="col-md-3 col-sm-3">Observaciones:</label>
                                <div class="col-md-8 col-sm-5">
                                    <textarea class="form-control" id="observaciones" name="observaciones" placeholder="Opcional"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<g:javascript>

$(document).ready(function() {

        $("#formulario").submit(function(e) {
            swal({
                type: 'info',
                title: "Subiendo archivo...",
                showConfirmButton: false
            });
       });

        function filterGlobal () {
        $('#aspirantes').DataTable().search(
            $('#buscar').val()
        ).draw();
}

        $('#buscar').on( 'keyup click', function () {
            filterGlobal();
        } );



        var table = $('#aspirantes').DataTable( {

            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": -1,
                    "data": null,
                    "defaultContent":
                    "<button class='detalle btn btn-default btn-xs'>Detalles</button>" +
                     "<button class='eliminar btn btn-danger btn-xs'>Eliminar</button>"
                }

            ],
            "ajax": {
                "url": "${createLink(controller: 'aspirante', action: 'getAspirantes')}",
                "type": "POST",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "periodo": $('#periodo').val()
                  } );
                }
            },
            "pageLength": 10,
            "language": {
                "decimal":        "",
                "emptyTable":     "No hay información disponible",
                "info":           "Mostrando del _START_ al _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 de 0 de 0 entradas",
                "infoFiltered":   "(filtrado de un total de _MAX_ entradas)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No se han encontrado resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar ordenación ascendente",
                    "sortDescending": ": Activar ordenación descendente"
                }
            },
            "sPaginationType": "full_numbers"


        } );

        $('#aspirantes tbody').on( 'click', '.eliminar', function () {
            var data = table.row( $(this).parents('tr') ).data()[0];
            eliminarAspirante(data);
        } );

        $('#aspirantes tbody').on( 'click', '.detalle', function () {
            var data = table.row( $(this).parents('tr') ).data()[0];
            location.href = "${createLink(action: 'detalle')}" + '/' + data;
        } );

        function eliminarAspirante(aspirante) {
            swal({   title: "Está seguro?",
                    text: "El aspirante y todo su contenido serán eliminados!",
                    type: "warning",
                    cancelButtonText: 'Cancelar',
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function() {
                    // ajax para eliminar
                    $.ajax({
                        type: 'POST',
                        data: {
                            id: aspirante
                        },
                        url: "${createLink(controller: 'aspirante', action: 'eliminar')}",
                        success: function(result) {
                            swal({
                                title: 'Bien',
                                text: result,
                                type: 'success',
                                confirmButtonText: 'Aceptar',
                                closeOnConfirm: true
                            }, function() {
                                table.ajax.reload();

                            });
                        },
                        error: function (error) {
                            sweetAlert("Ooops...", error.responseText, "error");
                        }
                    });

                });
        }


        window.periodo = function()  {
            table.ajax.reload();
        };

    } );




</g:javascript>
</body>
</html>