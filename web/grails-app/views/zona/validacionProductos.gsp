<%--
  Created by IntelliJ IDEA.
  User: giovanni
  Date: 3/05/17
  Time: 11:24 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='dashboard'/>
    <title></title>

</head>

<body>
<div class="input-group top_search col-md-6">
    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar...">

</div>

<div class="x_content">
    <div class="ln_solid"></div>
    <a href="${createLink(controller: 'producto', action: 'autorizarTodos')}" class="btn btn-primary btn-md">
        <i class="fa fa-file"></i> Autorizar todos los productos
    </a>
    <br>
    <table id="mercados" class="table table-striped responsive-utilities jambo_table">
        <thead>
        <tr class="headings">
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Categoría</th>
            <th>Mercado</th>
            <th>Precio</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${productos}" var="producto">
            <tr>
                <td>${producto?.id}</td>
                <td>${producto?.nombre}</td>
                <td>${producto?.descripcion}</td>
                <td>${producto?.categoria?.nombre}</td>
                <td>${producto?.mercado.nombre}</td>
                <td>${producto?.precio}</td>
                <td><a href="${createLink(controller: 'producto', action: 'rechazarProducto', id: producto?.id)}" class="btn btn-default btn-xs " data-toggle="tootlip" data-placement="top" title="Rechazar"><i class="fa fa-edit"></i></a></td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>

<g:javascript>

$(document).ready(function() {
    var hidden = true;



        function filterGlobal () {
        $('#mercados').DataTable().search(
            $('#buscar').val()
        ).draw();
}

        $('#buscar').on( 'keyup click', function () {
            filterGlobal();
        } );



        var table = $('#mercados').DataTable( {

            "autoWidth": true,
            "processing": true,
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },


            ],

            "pageLength": 10,
            "language": {
                "decimal":        "",
                "emptyTable":     "No hay información disponible",
                "info":           "Mostrando del _START_ al _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 de 0 de 0 entradas",
                "infoFiltered":   "(filtrado de un total de _MAX_ entradas)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No se han encontrado resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar ordenación ascendente",
                    "sortDescending": ": Activar ordenación descendente"
                }
            },
            "sPaginationType": "full_numbers"


        } );
// facilitando la vida de los desarrolladores: Fabric


    } );




</g:javascript>
</body>
</html>
