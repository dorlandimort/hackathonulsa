package hackathon

import mx.edu.ulsaoaxaca.hackathon.Categoria
import mx.edu.ulsaoaxaca.hackathon.Mercado
import mx.edu.ulsaoaxaca.hackathon.Producto
import org.json.simple.JSONObject

class ProductoService {

    def springSecurityService

    def guardar(def params) {
        def user = springSecurityService.getCurrentUser()
        Mercado mercado = Mercado.findByUsuario(user)
        Categoria categoria = Categoria.get(Long.parseLong(params.categoria))
        Producto p = new Producto(nombre: params.nombre, descripcion: params.descripcion, precio: params.precio,
                        mercado: mercado, categoria: categoria, autorizado: false)
        if(p.save()){
            p.url = "http://192.168.43.69:8080/Hackathon/producto/renderImage/" + p.id
            return([status: 200, message: "Producto creado correctamente."] as JSONObject)
        }else{
            return([status: 400, message: "El producto no se pudo crear."] as JSONObject)
        }
    }

    def editar(Long id, def params){
        Producto producto = Producto.get(id)
        producto.nombre = params.nombre
        producto.descripcion = params.descripcion
        producto.precio = Double.parseDouble(params.precio)
        Categoria categoria = Categoria.get(params.categoria)
        producto.categoria = categoria
        if(producto.save()){
            return([status: 200, message: "Producto editado correctamente."] as JSONObject)
        }else{
            return([status: 400, message: "El producto no se pudo editar."] as JSONObject)
        }
    }
}
