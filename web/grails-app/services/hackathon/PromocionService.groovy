package hackathon

import mx.edu.ulsaoaxaca.hackathon.Mercado
import mx.edu.ulsaoaxaca.hackathon.Promocion
import org.json.simple.JSONObject

class PromocionService {

    def springSecurityService

    def guardar(def params) {
        def user = springSecurityService.getCurrentUser()
        Mercado mercado = Mercado.findByUsuario(user)
        Promocion promocion = new Promocion(nombre: params.nombre, descripcion: params.descripcion,
                                    autorizado: false, mercado: mercado)
        if(promocion.save()){
            return([status: 200, message: "Promocion guardada correctamente."] as JSONObject)
        }else{
            println promocion.errors
            return([status: 400, message: "La promocion no se pudo guardar."] as JSONObject)
        }
    }

    def editar(Long id, def params){
        def promocion = Promocion.get(id)
        promocion.nombre = params.nombre
        promocion.descripcion = params.descripcion
        if(promocion.save()){
            return([status: 200, message: "Promocion editada correctamente."] as JSONObject)
        }else{
            println promocion.errors
            return([status: 400, message: "La promocion no se pudo editar."] as JSONObject)
        }
    }

}
