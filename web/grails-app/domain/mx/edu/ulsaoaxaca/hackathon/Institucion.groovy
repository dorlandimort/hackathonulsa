package mx.edu.ulsaoaxaca.hackathon

class Institucion {

    static hasMany = [zonas: Zona]

    String nombre
    String descripcion


    static constraints = {
        descripcion nullable: true, blank: true
    }

    static mapping = {
        version false
    }
}
