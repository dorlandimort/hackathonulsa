package mx.edu.ulsaoaxaca.hackathon

class Promocion {

    static belongsTo = [mercado: Mercado]

    String nombre
    String descripcion
    Boolean status = true
    Boolean autorizado = false
    Boolean rechazado = false

    static constraints = {
        descripcion nullable: true, blank: true
        status nullable: true
        autorizado nullable: true
    }

    static mapping = {
        version false
    }
}
