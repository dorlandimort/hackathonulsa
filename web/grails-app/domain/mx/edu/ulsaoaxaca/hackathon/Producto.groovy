package mx.edu.ulsaoaxaca.hackathon

class Producto {

    static belongsTo = [mercado: Mercado]

    String nombre
    String descripcion
    byte[] foto
    Double precio
    String url = "http://192.168.43.69:8080/Hackathon/producto/renderImage/" + this.id + ".jpg"

    Boolean status = true
    Boolean autorizado = false
    Boolean rechazado = false

    Categoria categoria

    static constraints = {
        descripcion nullable: true, blank: true
        foto nullable: true, maxSize: 1024 * 1024 * 2 // 2 megas

        status nullable: true
        autorizado nullable: true
        rechazado nullable: true
        url nullable: true, blank: true
    }

    static mapping = {
        version false
    }
}
