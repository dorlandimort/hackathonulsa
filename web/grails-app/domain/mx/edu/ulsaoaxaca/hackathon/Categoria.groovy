package mx.edu.ulsaoaxaca.hackathon

class Categoria {

    static belongsTo = [zona: Zona]
    static hasMany = [productos: Producto]

    String nombre
    String descripcion

    static constraints = {
        descripcion nullable: true, blank: true
    }

    static mapping = {
        version false
    }
}
