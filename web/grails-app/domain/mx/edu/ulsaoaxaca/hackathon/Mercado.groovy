package mx.edu.ulsaoaxaca.hackathon

class Mercado {

    static belongsTo = [zona: Zona]
    static hasMany = [productos: Producto, promociones: Promocion]

    String nombre
    String descripcion
    byte [] foto
    Double lat
    Double lng
    String direccion
    Usuario usuario

    static constraints = {
        descripcion nullable: true, blank: true
        foto nullable: true, maxSize: 1024 * 1024 * 2 // 2 megas

    }

    static mapping = {
        version false
    }

    int pendientes() {
        return Producto.countByAutorizadoAndRechazadoAndMercado(false, false, this)

    }
}
