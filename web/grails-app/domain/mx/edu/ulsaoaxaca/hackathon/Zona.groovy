package mx.edu.ulsaoaxaca.hackathon

class Zona {

    static belongsTo = [institucion: Institucion]
    static hasMany = [mercados: Mercado, categorias: Categoria]

    String nombre
    String descripcion

    static constraints = {
        descripcion nullable: true, blank: true
    }

    static mapping = {
        version false
    }
}
