package mx.edu.ulsaoaxaca.hackathon

import com.sun.org.apache.xml.internal.resolver.CatalogEntry
import org.springframework.security.access.annotation.Secured

class ZonaController {

    def springSecurityService

    @Secured(['ROLE_ADMIN', 'ROLE_MERCADO'])
    def mercados() {
        Usuario usuario = springSecurityService.currentUser

        if (usuario.authorities.any { it.authority == "ROLE_ADMIN"}) {
            Zona zona = Zona.get(1)
            [zona: zona]
        } else {
            redirect(controller: 'mercado', action: 'productos')
        }

    }

    @Secured(['ROLE_ADMIN'])
    def administrarMercados() {
        Zona zona = Zona.get(1)
        [zona: zona]
    }

    @Secured(['ROLE_ADMIN'])
    def guadarMercado() {
        Usuario user = new Usuario(username: params.username, password: params.password, enabled: true).save()
        Rol rol = Rol.findByAuthority("ROLE_MERCADO")
        UsuarioRol.create(user, rol, true)
        def mercado = new Mercado(params)
        mercado.usuario = user
        Zona zona = Zona.get(1)
        zona.addToMercados(mercado)
        if (zona.save())
            flash.message = "Mercado guardado correctamente"
        else
            flash.error = "Error al guardar mercado"
        redirect(controller: 'zona', action: 'administrarMercados')
    }

    @Secured(['ROLE_ADMIN'])
    def _modalEditarMercado(Long id) {
        println id
        def mercado = Mercado.get(id)
        [mercado: mercado]
    }

    @Secured(['ROLE_ADMIN'])
    def categoria() {
        Zona zona = Zona.get(1)
        [zona: zona]
    }

    @Secured(['ROLE_ADMIN'])
    def actualizarMercado(Long id) {
        def mercado = Mercado.get(id)
        mercado.properties = params
        if (mercado.save())
            flash.message = "Mercado actulizado correctamente"
        else
            flash.error = "Error al actulizar el mercado, verifique sus datos"

        Usuario user = mercado.usuario
        if (params.password)
            user.password = params.password
        user.username = params.username
        if (! user.save())
            flash.error = "Error al cambiar datos del usuario"
        redirect(controller: 'zona', action: 'administrarMercados')
    }

    @Secured(['ROLE_ADMIN'])
    def guardarCategoria() {
        Zona zona = Zona.get(1)
        def categoria = new Categoria(params)
        zona.addToCategorias(categoria)
        if (zona.save())
            flash.message = "Categoria agregada correctamente"
        else
            flash.error = "Error al guardar la categoria, verifique lo datos"
        redirect(controller: 'zona', action: 'categoria')
    }

    @Secured(['ROLE_ADMIN'])
    def _modalEditarCategoria(Long id) {
        def categoria = Categoria.get(id)
        [categoria: categoria]
    }

    @Secured(['ROLE_ADMIN'])
    def actualizarCategoria(Long id) {
        def categoria = Categoria.get(id)
        categoria.properties = params
        if (categoria.save())
            flash.message = "Mercado actulizado correctamente"
        else
            flash.error = "Error al actulizar el mercado, verifique sus datos"
        redirect(controller: 'zona', action: 'categoria')
    }

    @Secured(['ROLE_ADMIN'])
    def validacionProductos() {
        def productos = Producto.findAllByAutorizadoAndRechazado(false, false)
        [productos: productos]
    }

    @Secured(['ROLE_ADMIN'])
    def validacionPromociones() {
        def promociones = Promocion.findAllByAutorizadoAndRechazado(false, false)
        [promociones: promociones]
    }
}
