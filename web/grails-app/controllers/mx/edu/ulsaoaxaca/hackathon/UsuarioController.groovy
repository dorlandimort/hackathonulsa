package mx.edu.ulsaoaxaca.hackathon

import org.springframework.security.access.annotation.Secured

class UsuarioController {

    @Secured(['ROLE_ADMIN'])
    def validarUsuario() {
        def usuario
        if (params.id) {
            usuario = Usuario.findByIdAndUsername(params.id, params.username)
            if (usuario)
                render true
            else {
                usuario = Usuario.findByUsername(params.username)
                if (usuario)
                    render false
                else
                    render true
            }
        } else {
            usuario = Usuario.findByUsername(params.username)
            if (usuario)
                render false
            else
                render true
        }


    }
}
