package mx.edu.ulsaoaxaca.hackathon

import org.json.simple.JSONObject
import org.springframework.security.access.annotation.Secured

class PromocionController {

    def promocionService

    def index() { }

    @Secured(["ROLE_MERCADO"])
    def guardar(){
        JSONObject data = promocionService.guardar(params)
        if(data.status==200){
            flash.message = data.message
        }else{
            flash.error = data.message
        }
        redirect(controller: 'mercado', action: 'promociones')
    }

    @Secured(["ROLE_MERCADO"])
    def editar(Long id){
        JSONObject data = promocionService.editar(id, params)
        if(data.status==200){
            flash.message = data.message
        }else{
            flash.error = data.message
        }
        redirect(controller: 'mercado', action: 'promociones')
    }

    @Secured(["ROLE_MERCADO"])
    def _modalEditar(){
        Promocion promocion = Promocion.get(Long.parseLong(params.id))
        [promocion: promocion]
    }

    @Secured(["ROLE_ADMIN", "ROLE_MERCADO"])
    def rechazarPromocion(Long id) {
        def promocion = Promocion.get(id)
        promocion.rechazado = true
        if (promocion.save())
            flash.message = "Promocion rechazada correctamente"
        else
            flash.error = "Error al rechazar la promocion"
        redirect(controller: 'zona', action: 'validacionPromociones')
    }

    @Secured(["ROLE_ADMIN", "ROLE_MERCADO"])
    def autorizarTodos() {
        def promociones = Promocion.findAllByAutorizadoAndRechazado(false, false)
        promociones.each {
            it.autorizado = true
            it.save()
        }
        flash.message = "Producto autorizado correctamente"

        redirect(controller: 'zona', action: 'validacionPromociones')
    }

}
