package mx.edu.ulsaoaxaca.hackathon

import org.springframework.security.access.annotation.Secured

class MercadoController {

    def springSecurityService

    def index() { }

    @Secured(["ROLE_MERCADO"])
    def productos(){
        def user = springSecurityService.getCurrentUser()
        Mercado mercado = Mercado.findByUsuario(user)
        def productos = Producto.findAllByMercado(mercado)
        def categorias = Categoria.findAll()
        [productos: productos, categorias: categorias]
    }

    @Secured(["ROLE_MERCADO"])
    def promociones(){
        def user = springSecurityService.getCurrentUser()
        Mercado mercado = Mercado.findByUsuario(user)
        def promociones = Promocion.findAllByMercado(mercado)
        [promociones: promociones]
    }

}
