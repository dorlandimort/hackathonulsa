package mx.edu.ulsaoaxaca.hackathon

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONArray
import org.json.simple.JSONObject
import org.springframework.security.access.annotation.Secured

class ProductoController {

    def productoService

    def index() { }

    def getProductos(){
        def productos = Producto.findAllByAutorizado(true)
        def promociones = Promocion.findAllByAutorizado(true)
        JSONArray array = new JSONArray()
        int numP = 0
        int numPr = 0
        if(productos.size()>9){
            numP = 9
        }else{
            numP = productos.size()
        }

        if(promociones.size()>9){
            numPr = 9
        }else{
            numPr = promociones.size()
        }
        for(int i=0; i<numP; i++){
            JSONObject object = new JSONObject()
            object.put("nombre", productos.get(i).nombre)
            object.put("descripcion", productos.get(i).descripcion)
            object.put("precio", productos.get(i).precio)
            object.put("mercado", productos.get(i).mercado.nombre)
            object.put("categoria", productos.get(i).categoria.nombre)
            object.put("isPromo", false)
            object.put('lat', productos.get(i).mercado.lat)
            object.put('lng', productos.get(i).mercado.lng)
            object.put("url", productos.get(i).url)
            array.put(object)
        }
        for(int i=0; i<numPr; i++){
            JSONObject promo = new JSONObject()
            promo.put("nombre", promociones.get(i).nombre)
            promo.put("descripcion", promociones.get(i).descripcion)
            promo.put("mercado", promociones.get(i).mercado.nombre)
            promo.put("isPromo", true)
            promo.put('lat', productos.get(i).mercado.lat)
            promo.put('lng', productos.get(i).mercado.lng)
            promo.put("url", productos.get(i).url)
            array.put(promo)
        }
        render ([productos: array] as JSON)
    }

    def find(){


        def productos = Producto.createCriteria().list {
            eq('autorizado', true)
            or {
                like('nombre', "%${params.value}%")
                like('descripcion', "%${params.value}%")
            }
            order('precio', 'asc')
        }

        def promociones = Promocion.createCriteria().list {
            eq('autorizado', true)
            or {
                like('nombre', "%${params.value}%")
                like('descripcion', "%${params.value}%")
            }
        }

        JSONArray array = new JSONArray()
        for(int i=0; i<productos.size(); i++){
            JSONObject object = new JSONObject()
            object.put("nombre", productos.get(i).nombre)
            object.put("descripcion", productos.get(i).descripcion)
            object.put("precio", productos.get(i).precio)
            object.put("mercado", productos.get(i).mercado.nombre)
            object.put("categoria", productos.get(i).categoria.nombre)
            object.put("isPromo", false)
            object.put('lat', productos.get(i).mercado.lat)
            object.put('lng', productos.get(i).mercado.lng)
            object.put("url", productos.get(i).url)
            array.put(object)
        }
        for(int i=0; i<promociones.size(); i++){
            JSONObject promo = new JSONObject()
            promo.put("nombre", promociones.get(i).nombre)
            promo.put("descripcion", promociones.get(i).descripcion)
            promo.put("mercado", promociones.get(i).mercado.nombre)
            promo.put("isPromo", true)
            promo.put('lat', productos.get(i).mercado.lat)
            promo.put('lng', productos.get(i).mercado.lng)
            promo.put("url", productos.get(i).url)
            array.put(promo)
        }
        render ([productos: array] as JSON)
    }

    @Secured(["ROLE_MERCADO"])
    def guardar(){
        JSONObject data = productoService.guardar(params)
        if(data.status == 200){
            flash.message = data.message
            redirect(controller: 'mercado', action: 'productos')
        }else{
            flash.error = data.message
            redirect(controller: 'mercado', action: 'productos')
        }
    }

    @Secured(["ROLE_MERCADO"])
    def editar(Long id){
        JSONObject data = productoService.editar(id, params)
        if(data.status == 200){
            flash.message = data.message
            redirect(controller: 'mercado', action: 'productos')
        }else{
            flash.error = data.message
            redirect(controller: 'mercado', action: 'productos')
        }
    }

    @Secured(["ROLE_MERCADO"])
    def _modalEditar(){
        def producto = Producto.get(Long.parseLong(params.id))
        def categorias = Categoria.findAll()
        [producto: producto, categorias: categorias]
    }

    @Secured(["ROLE_ADMIN", "ROLE_MERCADO"])
    def rechazarProducto(Long id) {
        def producto = Producto.get(id)
        producto.rechazado = true
        if (producto.save())
            flash.message = "Producto rechazado correctamente"
        else
            flash.error = "Error al rechazar el producto"
        redirect(controller: 'zona', action: 'validacionProductos')
    }

    @Secured(["ROLE_ADMIN", "ROLE_MERCADO"])
    def autorizarTodos() {
        def productos = Producto.findAllByAutorizadoAndRechazado(false, false)
        productos.each {
            it.autorizado = true
            it.save()
        }
            flash.message = "Producto autorizado correctamente"

        redirect(controller: 'zona', action: 'validacionProductos')
    }

    def renderImage(Long id) {
        Producto producto = Producto.get(id)
        File file
        if (producto) {
            file = new File("/home/alejandro/images/" + producto.nombre + ".jpg")
            if (! file.exists()) {
                file = new File("/home/alejandro/images/default.jpg")
            }
            response.outputStream << file.bytes
        } else {
            file = new File("/home/alejandro/images/default.jpg")
            response.outputStream << file.bytes
        }



    }

}
